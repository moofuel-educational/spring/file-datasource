package com.moofuel.edu.datasource.impl;

import com.moofuel.edu.datasource.FileDataSource;
import com.moofuel.edu.datasource.config.FileDataSourceConfiguration;
import com.moofuel.edu.datasource.model.EntryInfo;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Дмитрий
 * @since 26.07.2017
 */
public class DefaultFileDataSource implements FileDataSource {

    private static final String EMPTY_STRING = "";
    private final FileDataSourceConfiguration innerConfig;
    private final PrintWriter printWriter;

    public DefaultFileDataSource(FileDataSourceConfiguration config) throws IOException {
        this.innerConfig = config;
        final Path loggerPath = innerConfig.getDataSourcePath();
        if (!loggerPath.toFile().exists()) {
            Files.createFile(loggerPath);
        }
        final FileOutputStream fileOutputStream = new FileOutputStream(loggerPath.toFile(), true);
        final OutputStreamWriter fileWriter = new OutputStreamWriter(fileOutputStream, StandardCharsets.UTF_8);
        final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        printWriter = new PrintWriter(bufferedWriter);
    }

    @Override
    public EntryInfo writeEntry(Integer lineNumber, String message) throws IOException {
        final String finalMessage = composeEntry(message);
        final Path loggerPath = innerConfig.getDataSourcePath();
        if (lineNumber != null && lineNumber > 0) {
            final List<String> lines = Files.readAllLines(loggerPath);
            if (lineNumber > lines.size() - 1) {
                appendLineToEndOfFile(finalMessage);
                return new EntryInfo(lines.size(), finalMessage);
            }
            final int index = lineNumber - 1;
            lines.set(index, finalMessage);
            Files.write(loggerPath, lines);
            return new EntryInfo(index, finalMessage);

        } else {
            appendLineToEndOfFile(finalMessage);
            try (Stream<String> lines = Files.lines(loggerPath)) {
                final Long lineCountMinusOne = lines.count() - 1;
                return new EntryInfo(lineCountMinusOne.intValue(), finalMessage);
            }
        }
    }

    @Override
    public String readEntry(Integer lineNumber) throws IOException {
        final Stream<String> lines = Files.lines(innerConfig.getDataSourcePath());
        final Optional<String> foundString = lines.skip(lineNumber).findFirst();
        return foundString.orElse(EMPTY_STRING);
    }

    private void appendLineToEndOfFile(String finalMessage) {
        printWriter.println(finalMessage);
        printWriter.flush();
    }

    private String composeEntry(final String message) {
        String processedMessage = Optional.ofNullable(message)
                .orElse("null")
                .replaceAll(System.lineSeparator(), "");
        final String prefix = innerConfig.getPrefix();
        final String postfix = innerConfig.getPostfix();
        return prefix + processedMessage + postfix;
    }


}
