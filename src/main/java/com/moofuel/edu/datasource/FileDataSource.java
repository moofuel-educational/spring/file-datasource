package com.moofuel.edu.datasource;

import com.moofuel.edu.datasource.model.EntryInfo;

import java.io.IOException;

/**
 * @author Дмитрий
 * @since 26.07.2017
 */
public interface FileDataSource {

    EntryInfo writeEntry(Integer lineNumber, String message) throws IOException;

    String readEntry(Integer lineNumber) throws IOException;
}
