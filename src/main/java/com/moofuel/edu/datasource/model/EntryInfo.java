package com.moofuel.edu.datasource.model;

/**
 * @author Дмитрий
 * @since 26.07.2017
 */
public class EntryInfo {

    private Integer lineNumber;
    private String message;

    /**
     * @param lineNumber - number of the line in file, starting from 0, which represents message
     * @param message    - actual message
     */
    public EntryInfo(Integer lineNumber, String message) {
        this.lineNumber = lineNumber;
        this.message = message;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "EntryInfo{" +
                "lineNumber=" + lineNumber +
                ", message='" + message + '\'' +
                '}';
    }
}
