package com.moofuel.edu.datasource.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Дмитрий
 * @since 26.07.2017
 */
public class FileDataSourceConfiguration {

    private Path dataSourcePath;
    private String prefix;
    private String postfix;

    public Path getDataSourcePath() {
        return dataSourcePath;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getPostfix() {
        return postfix;
    }

    @Override
    public String toString() {
        return "FileDataSourceConfiguration{" +
                "dataSourcePath=" + dataSourcePath +
                ", prefix='" + prefix + '\'' +
                ", postfix='" + postfix + '\'' +
                '}';
    }

    private FileDataSourceConfiguration(Path dataSourcePath, String prefix, String postfix) {
        this.dataSourcePath = dataSourcePath;
        this.prefix = prefix;
        this.postfix = postfix;
    }

    public static ValeraConfigBuilder builder() {
        return new ValeraConfigBuilder();
    }

    public static class ValeraConfigBuilder {
        private Path dataSourcePath;
        private String prefix;
        private String postfix;

        private ValeraConfigBuilder() {
        }

        public ValeraConfigBuilder addDataSourcePath(String dataSourcePath) {
            final Path path = Paths.get(dataSourcePath);
            try {
                if (path.toFile().isDirectory()) {
                    Files.createDirectories(path);
                } else {
                    Files.createDirectories(path.getParent());
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not create directories", e);
            }
            this.dataSourcePath = path;
            return this;
        }

        public ValeraConfigBuilder addPrefix(String suffix) {
            this.prefix = suffix;
            return this;
        }

        public ValeraConfigBuilder addPostfix(String postfix) {
            this.postfix = postfix;
            return this;
        }

        public FileDataSourceConfiguration build() {
            return new FileDataSourceConfiguration(dataSourcePath, prefix, postfix);
        }

        @Override
        public String toString() {
            return "ValeraConfigBuilder{" +
                    "dataSourcePath=" + dataSourcePath +
                    ", prefix='" + prefix + '\'' +
                    ", postfix='" + postfix + '\'' +
                    '}';
        }
    }
}
